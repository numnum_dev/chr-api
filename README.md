# README #

This project is web application server for chr, NumnuM team.

### Stack overview ###

* Java 8 + Spring 4.1.1
* MySQL 5.7
* ElasticSearch 2.3.2
* Redis 3.2.0
* spring-data-redis 1.7.1

### Team NumnuM ###

* Genie Youn 
* Hyesoo Jang
* Yujin Jang

### API Docs ###

**Stroe** 
     
```
#!curl

curl -X GET /curations
```

*Parameters*

| Element    | Optional   | Type  |   Description       |
| -----------|:-----------| ------|:--------------------|
|      -     |      -     |   -   |          -          |

*Response*
```
#!json
[
  {
    "seqStore": 1,
    "storeName": "봉구스 밥버거",
    "star": "4.0",
    "status": "normal",
    "tag": [
      "한식",
      "점심"
    ],
    "imgUrl": null
  },
  {
    "seqStore": 2,
    "storeName": "미미네",
    "star": "1.0",
    "status": "busy",
    "tag": [
      "한식",
      "점심"
    ],
    "imgUrl": null
  },
  {
    "seqStore": 3,
    "storeName": "유진이네",
    "star": "2.0",
    "status": "normal",
    "tag": [
      "한식",
      "점심"
    ],
    "imgUrl": null
  }
]
```
**Star** 
     
```
#!curl

curl -X GET /stars/{seqStore}
```

*Parameters*

| Element    | Optional   | Type  |   Description       |
| -----------|:-----------| ------|:--------------------|
|  seqStore  |   false    | double|   매장 고유 번호     |

*Response*
```
#!json
[
  {
    "seqStar": 3,
    "seqAccount": 1,
    "seqStore": 1,
    "score": 2,
    "createTime": 1465974674000
  },
  {
    "seqStar": 2,
    "seqAccount": 1,
    "seqStore": 1,
    "score": 4,
    "createTime": 1465974669000
  },
  {
    "seqStar": 1,
    "seqAccount": 1,
    "seqStore": 1,
    "score": 3,
    "createTime": 1465974457000
  }
]
```
*별점 주기*
```
#!curl

curl -X POST /stars/{seqStore}
```

*Parameters*

| Element    | Optional   | Type  |   Description       |
| -----------|:-----------| ------|:--------------------|
|  seqStore  |   false    | double|   매장 고유 번호     |

*RequestBody*

| Element    | Optional   | Type  |   Description       |
| -----------|:-----------| ------|:--------------------|
|    star    |   false    | double|   별점 0.0 ~ 5.0    |
*Response*
```
#!json
*성공*
{
  "result": "success",
  "code": 1,
  "message": "별점이 정상적으로 등록되었습니다."
}
*실패*
{
  "result": "fail",
  "code": 2,
  "message": "별점 등록 오류."
}
```
*후기 등록*
```
#!curl

curl -X POST /stores/{seqStore}/reviews
```

*Parameters*

| Element    | Optional   | Type  |   Description       |
| -----------|:-----------| ------|:--------------------|
|  seqStore  |   false    | double|   매장 고유 번호     |

*RequestBody*

| Element    | Optional   | Type  |   Description       |
| -----------|:-----------| ------|:--------------------|
|    score   |   true     | double|별점0.0~5.0 (기본 3.0 )|
|   message  |   false    | String|   후기내용 (최대255)   |
*Response*
```
#!json
*성공*
{
  "result": "success",
  "code": 1,
  "message": "후기가 정상적으로 등록되었습니다."
}
*실패*
{
  "result": "fail",
  "code": 2,
  "message": "후기 등록에 실패하였습니다."
}
```
*MyPage 가져오기*
```
#!curl

curl -X GET /users/mypage
```

*Parameters*

*RequestBody*

*Response*
```
#!json
*result*
{
  "seqAccount": 0,
  "name": "guest",
  "email": "guest",
  "bcPassword": "test",
  "salt": 1,
  "profileImg": null
}
```

*로그인*
```
#!curl

curl -X GET /users/login
```

*Parameters*

*RequestBody*
```
#!json
{
  "email": "guest",
  "bcPassword": "test"
}
```

*Response*
```
#!json
*성공*
{
  "code": 200,
  "message": "완료",
  "statusCode": 200
}
*실패*
{
  "code": 997,
  "message": "가입되지 않은 사용자",
  "statusCode": 500
}
{
  "code": 996,
  "message": "패스워드 불일치",
  "statusCode": 500
}
```