package com.tcp.chr.mappers;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository("starMapper")
public class StarMapper extends AbstractMapper {

	private static final Logger LOG = LoggerFactory.getLogger(StarMapper.class);
	
	public List getStarBySeqStore(double seqStore, int num){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("seqStore", seqStore);
		map.put("maxNum",num);					//최대 갯수
		return selectList("Star.selectStarBySeqStore", map);
	}
	
	public Object setStar(double seqStore, double seqAccount, double star){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("seqStore", seqStore);
		map.put("seqAccount", seqAccount);
		map.put("star", star);
		return insert("Star.insertStar", map);
	}
	
}
