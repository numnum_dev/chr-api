package com.tcp.chr.mappers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository("storeMapper")
public class StoreMapper extends AbstractMapper {

	private static final Logger LOG = LoggerFactory.getLogger(StoreMapper.class);
	
	// 후기 등록
	public Object setReview(HashMap map){
		return insert("Store.insertReview", map);
	}
}
