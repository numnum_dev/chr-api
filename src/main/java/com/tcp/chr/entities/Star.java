package com.tcp.chr.entities;

import java.io.Serializable;
import java.sql.Timestamp;

public class Star implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1560685974803540931L;

	private Long seqStar;
	private Long seqAccount;
	private Long seqStore;
	private double score;
	private Timestamp createTime;
	
	public Long getSeqStar() {
		return seqStar;
	}
	
	public Long getSeqAccount() {
		return seqAccount;
	}
	public void setSeqAccount(Long seqAccount) {
		this.seqAccount = seqAccount;
	}
	public Long getSeqStore() {
		return seqStore;
	}
	public void setSeqStore(Long seqStore) {
		this.seqStore = seqStore;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	
}
