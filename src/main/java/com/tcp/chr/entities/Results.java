package com.tcp.chr.entities;

public enum Results {
		
	SUCCESS(200,"완료",200),
	REDIS_SHUTDOWN_ERROR(998, "레디스에서 데이터를 가져올 수 없습니다", 500),
	NOT_JOINED_USER(997, "가입되지 않은 사용자",500),
	PASSWORD_NOT_CORRECTED(996,"패스워드 불일치", 500);
	
	int code;
	String message;
	int statusCode;
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	Results(int code, String message) {
		this.code = code;
		this.message = message;
		this.statusCode = 400;
	}
	
	Results(int code, String message, int statusCode) {
		this.code = code;
		this.message = message;
		this.statusCode = statusCode;
	}
}
