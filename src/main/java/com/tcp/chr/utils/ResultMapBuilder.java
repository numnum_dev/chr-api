package com.tcp.chr.utils;

import java.util.HashMap;
import com.tcp.chr.entities.Results;
/**
 * ResultMapBuilder
 * 
 * @author genie
 * @date 2016.09.08
 */
public class ResultMapBuilder {

	private volatile static ResultMapBuilder instance;
	private volatile static HashMap<String,Object> resultMap = new HashMap<String,Object>();
	private ResultMapBuilder() {}; //기본 생성자 제거
	
	/**
	 * Singleton
	 * 
	 * @return instance
	 */
	public static synchronized ResultMapBuilder getInstance() {
		
		if(instance == null) {
			instance = new ResultMapBuilder();
		}
		
		return instance;
	}
	/**
	 * error enum을 hashmap 으로 파싱한다.
	 * 
	 * @param error
	 * @return hashmap
	 */
	public static synchronized HashMap<String, Object> setResult(Results result) {
		
		resultMap.put("code", result.getCode());
		resultMap.put("message", result.getMessage());
		resultMap.put("statusCode", result.getStatusCode());
		
		return resultMap;
	}
}
