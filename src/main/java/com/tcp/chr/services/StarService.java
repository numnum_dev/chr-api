package com.tcp.chr.services;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tcp.chr.entities.Account;
import com.tcp.chr.mappers.StarMapper;

@Service("starService")
public class StarService {

	private static final Logger LOG = LoggerFactory.getLogger(StarService.class);
	private static final int MAX_NUM = 50;

	@Resource(name = "starMapper")
	private StarMapper starMapper;

	public List getStarList(HttpServletRequest req, Double seqStore) {
		List starList = starMapper.getStarBySeqStore(seqStore, MAX_NUM);
		return starList;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	public Object setStar(HttpServletRequest req, HttpServletResponse res, Double seqStore, Double star) {
		Account ac = (Account) req.getSession().getAttribute("account");
		Object result = starMapper.setStar(seqStore, ac.getSeqAccount(), star);
		
		HashMap<String, Object> json = new HashMap<String, Object>();
		
		if (result.equals(1)) {
			json.put("message", "별점이 정상적으로 등록되었습니다.");
			json.put("code", 1);
			json.put("result", "success");
		} else {
			json.put("message", "별점 등록 오류");
			json.put("code", 2);
			json.put("result", "fail");
		}
		return json;
	}
}
