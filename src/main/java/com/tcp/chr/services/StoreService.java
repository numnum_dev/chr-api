package com.tcp.chr.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tcp.chr.entities.Account;
import com.tcp.chr.entities.Results;
import com.tcp.chr.mappers.StoreMapper;

@Service("storeService")
public class StoreService {

	private static final Logger LOG = LoggerFactory.getLogger(StoreService.class);

	@Autowired
	RedisTemplate<Long, List> redisTemplate;
	
	@Resource(name="redisTemplate")
	private ValueOperations<Long, List> valueOps;
	
	@Resource(name="storeMapper")
	private StoreMapper storeMapper;
	
	/**
	 * redis에서 사용자의 seqAccount값으로 추천목록을 가져온다.
	 * 
	 * @param req
	 * @return List<Store>
	 */
	public List getCurationList(HttpServletRequest req){
		Account ac = (Account)req.getSession().getAttribute("account");
		try{
			List curationList = valueOps.get(ac.getSeqAccount());
			return curationList;
		} catch (Exception e) {
			List curationList = new ArrayList<Results>();
			curationList.add(Results.REDIS_SHUTDOWN_ERROR);
			return curationList;
		}
	}
	
	/**
	 * 후기를 등록한다.
	 * 
	 * @param seqStore
	 * @param data {"message", "score"}
	 * @return 1 (success) 
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public Object setReview(HttpServletRequest req, HttpServletResponse res, double seqStore, HashMap data){
		
		HashMap map = new HashMap<String, Object>();
		Account ac = (Account) req.getSession().getAttribute("account");
		
		if(data.get("score") == null){
			data.put("score", 3.0);
		}
		
		if(data.get("message") == null){
			throw new NullPointerException("메세지가 없습니다.");
		}
		
		map.put("seqAccount", ac.getSeqAccount());
		map.put("score", data.get("score"));
		map.put("message", data.get("message"));
		map.put("seqStore", seqStore);
		
		return storeMapper.setReview(map);
	}
}
