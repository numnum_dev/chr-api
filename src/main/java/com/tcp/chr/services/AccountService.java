
package com.tcp.chr.services;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tcp.chr.entities.Account;
import com.tcp.chr.entities.Results;
import com.tcp.chr.mappers.AccountMapper;

/**
 * 계정 관련 서비스 
 * 
 * @author geine
 * @date 2016-07-15
 * @since 1.0
 */
@Service("accountService")
public class AccountService {

	private static final Logger LOG = LoggerFactory.getLogger(AccountService.class);

	@Resource(name = "accountMapper")
	private AccountMapper accountMapper;

	/**
	 * myPage 가져오기 API 
	 * @return 자신의 Account 객체
	 * @see com.tcp.chr.entities.Account
	 */
	public Account getMypage(HttpServletRequest req, HttpServletResponse res) {

		double seqAccount = (Double) req.getSession().getAttribute("seqAccount");
		Account ac = accountMapper.getAccountBySeq(seqAccount);

		return ac;
	}

	/**
	 * 로그인 API 
	 * @param Account ac
	 */
	public Results doLogin(HttpServletRequest req, HttpServletResponse res, Account ac) {

		Account sAc = accountMapper.getAccountByEmail(ac.getEmail());

		if (sAc == null) {
			//없는 사용자 계정
			return Results.NOT_JOINED_USER;
		}
		else {
			if (sAc.getBcPassword().equals(ac.getBcPassword())) {
				LOG.info("로그인성공");
				sAc.setBcPassword("");
				req.getSession().setAttribute("account", sAc);
				return Results.SUCCESS;
			}
			else {
				return Results.PASSWORD_NOT_CORRECTED;
			}
		}
	}
}
