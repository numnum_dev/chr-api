package com.tcp.chr.controllers;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tcp.chr.annotations.API;
import com.tcp.chr.annotations.API.Useage;
import com.tcp.chr.annotations.AccessRole;
import com.tcp.chr.annotations.AccessRole.Role;
import com.tcp.chr.entities.Account;
import com.tcp.chr.services.AccountService;
import com.tcp.chr.utils.ResultMapBuilder;

/**
 * 계정 관련 Controller
 * 
 * @author geine
 * @date 2016-07-15
 * @since 1.0
 */
@Controller
public class AccountController extends AbstractController {

	private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);
	
	@Resource(name = "accountService")
	private AccountService accountService;
	
	/**
	 * myPage 가져오기 API
	 * nullpointexception 처리 필요 없을것같다.
	 */
	@API(Useage.Experimental)
	@AccessRole(role = Role.selector)
	@RequestMapping(value = "/users/mypage", method = RequestMethod.GET)
	public void getMyPage(HttpServletRequest req, HttpServletResponse res){
		Account ac = accountService.getMypage(req, res);
		sendResponse(ac, res);
	}
	/**
	 * 로그인 API
	 * @param Account ac
	 */
	@API(Useage.Experimental)
	@AccessRole(role = Role.all)
	@RequestMapping(value = "/users/login", method = RequestMethod.POST, consumes = "application/json")
	public void doLogin(HttpServletRequest req, HttpServletResponse res, @RequestBody Account ac){
		sendResponse(ResultMapBuilder.getInstance().setResult(accountService.doLogin(req, res, ac)), res);
	}
}
