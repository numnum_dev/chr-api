package com.tcp.chr.controllers;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tcp.chr.annotations.API;
import com.tcp.chr.annotations.AccessRole;
import com.tcp.chr.annotations.API.Useage;
import com.tcp.chr.annotations.AccessRole.Role;
import com.tcp.chr.services.StarService;

@Controller
public class StarController extends AbstractController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StarController.class);
	
	@Resource(name="starService")
	private StarService starService;
	
	/**
	 * 최근 별점을 가져온다. 50개
	 * 
	 * @param seqStore
	 * @author genie
	 * @date 2016.06.15
	 * @since 1.0
	 */
	@API(Useage.Experimental)
	@AccessRole(role = Role.all)
	@RequestMapping(value = "/stars/{seqStore}", method = RequestMethod.GET)
	public void getStarList(HttpServletRequest req, HttpServletResponse res, @PathVariable Double seqStore){
		List starList = starService.getStarList(req, seqStore);
		sendResponse(starList, res);
	}
	
	/**
	 * 별점을 부여한다. 동일 사용자의 중복 부여처리 해야한다.
	 * 
	 * @param seqStore
	 * @param star
	 * @author genie
	 * @date 2016.06.23
	 * @since 1.0
	 */
	@API(Useage.Experimental)
	@AccessRole(role = Role.author)
	@RequestMapping(value = "/stars/{seqStore}", method = RequestMethod.POST, consumes = "application/json")
	public void setStar(HttpServletRequest req, HttpServletResponse res, @PathVariable Double seqStore, @RequestBody HashMap data){
		Object result = starService.setStar(req, res, seqStore, (Double) data.get("star"));
		sendResponse(result, res);
	}
	
}	
